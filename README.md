### BRIQUE Analytics

------



BA(BRIQUE Analytics)는 쉽고 편리한 데이터 분석 환경을 제공하는 **통합 분석 플랫폼**으로써,



웹 기반의 UI를 통해

- 목적별 분석 워크플로우의 작성
- 온라인 테스트
- 분석결과 확인

등의 다양한 기능을 제공합니다



------

#### 구성

본 리파지토리에는 BA의 설치와 사용에 대한 이해를 돕기위한 매뉴얼 및 튜토리얼을 포함하고 있습니다



- 매뉴얼

  - 설치 매뉴얼

    - 시스템 요구사양
    - 설치가이드

    

  - 사용 매뉴얼

    - 기본기능
    - 작성방법
    - 기능활용
    - 사용툴팁

  

- 튜토리얼

  - Getting Started
  - How to make Library
  - How to make Workflow
  - How to make Resource

  

  각 튜토리얼은 다운로드 받은 뒤, stroy.html을 실행함으로써, 따라하기 방식으로 진행할 수 있습니다



